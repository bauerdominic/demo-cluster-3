#!/bin/bash

set -e

source variables.sh
source common.sh

usage() {
    stderr "Usage: env GITLAB_TOKEN=... $(basename "$0")"
}

if [ -z "$GITLAB_TOKEN" ]; then
    usage
    exit 1
fi

delete_agent() {
    query="mutation deleteAgent {
      clusterAgentDelete(input: {id: \"$1\"}) {
        errors
      }
    }"
    request "$GRAPHQL_URL" -d "$(body "$query")" || exit 1
}

id=$(cat "$AGENT_ID_FILE")
stderr "Deleting agent $id"
delete_agent "$id"
