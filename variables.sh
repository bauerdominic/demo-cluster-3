#!/bin/bash
# shellcheck disable=SC2034

GITLAB_HOST="${GITLAB_HOST:-$CI_SERVER_HOST}"
GITLAB_HOST=${GITLAB_HOST:-gitlab.com}
PROTO="${CI_SERVER_PROTOCOL:-https}"
PORT="${CI_SERVER_PORT:-443}"
BASE="${PROTO}://${GITLAB_HOST}"
if [ "$PORT" != 443 ] && [ "$PORT" != 80 ]; then
    # Should probably also check proto... but who runs HTTPS on port 80 or vice-versa?
    BASE="${BASE}:${PORT}"
fi

REST_URL="${BASE}/api/v4"
GRAPHQL_URL="${BASE}/api/graphql"
AGENT_ID_FILE="./agentid"

if [ -n "$KUBECONFIG" ]; then
    export KUBECONFIG="$KUBECONFIG"
elif [ -f "$HOME/.kube/config" ]; then
    export KUBECONFIG="$HOME/.kube/config"
else
    export KUBECONFIG="/var/run/kubeconfig"
fi
