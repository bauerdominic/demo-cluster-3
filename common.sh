#!/bin/bash

source variables.sh

stderr() {
    echo "$1" > /dev/stderr
}

request() {
    curl -sSL \
         --header "Authorization: Bearer $GITLAB_TOKEN"\
         --header "Content-Type: application/json"\
         "$@"
}

body() {
    jq -c --null-input --arg query "$1" '.query=$query'
}
